#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

Adafruit_BME280 bme;

void setup() {
  Serial.begin(9600);
  if (bme.begin(0x76) == false) Serial.println("[DEBUG] Could not find any BME280 sensor!");
}


void loop() {  
  float temperature = bme.readTemperature();
  float fahrenheit = temperature != 0 ? temperature * 1.8 + 32 : 0;
  float humidity = bme.readHumidity();
  float pascal = bme.readPressure() / 100;
  float bar = pascal / 1000;

  Serial.println("Temperature: " + getTemperatureString(temperature));
  Serial.println("Fahrenheit: " + getFahrenheitString(fahrenheit));
  Serial.println("Humidity: " + getHumidityString(humidity));
  Serial.println("Pascal: " + getPascalString(pascal));
  Serial.println("Bar: " + getBarString(bar));
  Serial.println("================================");
            
  delay(1000);   
}

String getTemperatureString(float temperature) {
  return String(temperature) + " °C";
}

String getFahrenheitString(float fahrenheit) {
  return String(fahrenheit) + " °F";
}

String getHumidityString(float humidity) {
  return String(humidity) + " %";
}

String getPascalString(float pascal) {
  return String(pascal) + " hPa";
}

String getBarString(float bar) {
  return String(bar) + " bar";
}
